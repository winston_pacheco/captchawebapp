﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace captchaJavascript.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ValidateCaptcha(string challengeValue, string responseValue)
        {
            var captchaValidator = new Recaptcha.RecaptchaValidator
            {
                PrivateKey = ConfigurationManager.AppSettings["recaptchaPrivateKey"],
                RemoteIP = HttpContext.Request.UserHostAddress,
                Challenge = challengeValue,
                Response = responseValue
            };

            var recaptchaResponse = captchaValidator.Validate(); // Send data about captcha validation to reCAPTCHA site.
            return Json(recaptchaResponse.IsValid); // Get boolean value about Captcha success / failure.
        }
    }
}
